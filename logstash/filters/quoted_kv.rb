require "logstash/filters/base"
require "logstash/namespace"

# This filter helps automatically parse messages (or specific event fields)
# which are of the '"foo bar"="bar foo"' variety.
#
# For example, if you have a log message which contains 'ip=1.2.3.4 
# "destination port"=80 details="web server" 
# "error message"="Data is invalid"', you can parse those automatically 
# by configuring:
#
# filter {
#   quoted_kv { }
# }
#
# The above will result in a message of 'ip=1.2.3.4 "destination port"=80 
# details="web server" "error message"="Data is invalid"' having
# the fields:
#
# * "ip": 1.2.3.4
# * "destination port": 80
# * "details": "web server"
# * "error message": "Data is invalid"
#
# This is great for logs that have a "key"="value" syntax
#
# You can configure any arbitrary strings to split your data on,
# in case your data is not structured using '=' signs and whitespace.
# For example, this filter can also be used to parse query parameters like
# '"error message":"Data is invalid" by setting the `value_split` parameter to ":".
class LogStash::Filters::QuotedKeyValue < LogStash::Filters::Base
    # This is how you call it from the config
    config_name "quoted_kv"
    
    milestone 1
    
    # A string of characters to use as delimiters for identifying key-value relations.
    #
    # These characters form a regex character class and thus you must escape special regex
    # characters like '[' or ']' using '\'.
    #
    # For example, to identify key-values such as
    # 'key1:value1 key2:value2':
    #
    # filter { quoted_kv { value_split => ":" } }
    config :value_split, :validate => :string, :default => '='
    
    # A string of characters to use as delimiters for identifying the beggining and end
    # of the keys and values. The first char is taken as the initial char of the key/value
    # and the second is taken as the final char.
    #
    # These characters form a regex character class and thus you must escape special regex
    # characters like '[' or ']' using '\'.
    #
    # For example, to identify key-values such as
    # '[big key]=[big value]':
    #
    # filter { quoted_kv { quote_chars => "[]" } }
    config :quote_chars, :validate => :string, :default => '""'
    
    # A string to prepend to all of the extracted keys.
    #
    # For example, to prepend arg_ to all keys:
    #
    # filter { quoted_kv { prefix => "arg_" } }
    config :prefix, :validate => :string, :default => ''
    
    # The field to perform 'key=value' searching on
    #
    # For example, to process the `not_the_message` field:
    #
    # filter { quoted_kv { source => "not_the_message" } }
    config :source, :validate => :string, :default => "message"
    
    # A string of characters to trim from the value. This is useful if your
    # values are wrapped in brackets or are terminated with commas (like postfix
    # logs).
    #
    # These characters form a regex character class and thus you must escape special regex
    # characters like '[' or ']' using '\'.
    #
    # For example, to strip '<', '>', '[', ']' and ',' characters from values:
    #
    # filter { quoted_kv { trim => "<>\[\]," } }
    config :trim, :validate => :string
    
    # A string of characters to trim from the end of the value. This is useful if your
    # values are terminated with commas (like postfix logs).
    #
    # These characters form a regex character class and thus you must escape special regex
    # characters like '[' or ']' using '\'.
    #
    # For example, to strip '<', '>', '[', ']' and ',' characters from the end of values:
    #
    # filter { quoted_kv { trim => "<>\[\]," } }
    config :trim_end, :validate => :string
    
    public
    def register
        # nothing to do
    end
    
    public
    def filter(event)
        # Return if there is no event
        return unless filter?(event)
        if event[@source]
            # There is a message, replace stuff
            event[@source].scan(/((#{@quote_chars[0]}(?<key1>.*?[^\\])#{@quote_chars[1]})|(?<key2>[^ ]+))\s*#{@value_split}\s*((#{@quote_chars[0]}(?<value1>.*?[^\\])#{@quote_chars[1]})|(?<value2>[^ ]+))/) do |key1, key2, value1, value2|
                final_key = ''
                if !key1.nil?
                    final_key = key1
                elsif !key2.nil?
                    final_key = key2
                end # keys if
                
                final_value = ''
                if !value1.nil?
                    final_value = value1
                elsif !value2.nil?
                    final_value = value2
                end # values if
                
                if !@trim.nil?
                    final_value = final_value.gsub(/[#{@trim}]/, '')
                end
                
                if !@trim_end.nil?
                    final_value = final_value.gsub(/[#{@trim_end}]+$/, '')
                end

                if final_key != ''
                    event[@prefix + final_key] = final_value
                end
            end # message scan
            filter_matched(event)
        else
            event.cancel
        end # message if
    end # def filter 
end # class LogStash::Filters::QuotedKeyValue
